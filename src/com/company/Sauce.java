package com.company;

public class Sauce {

    //TODO faire en sorte que la sauce soit un ingrédient
    private String name;
    public Boolean isVegetarian;
    public Boolean isPescetarian;

    public Sauce(String name, Boolean isVegetarian, Boolean isPescetarian){
        this.name = name;
        this.isVegetarian = isVegetarian;
        this.isPescetarian = isPescetarian;
    }
}
