package com.company;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        Ingredient salade = new Ingredient("salade", true, true);
        Ingredient tomate = new Ingredient("tomate", true, true);
        Ingredient oignon = new Ingredient("oignon", true, true);
        Ingredient poisson = new Ingredient("poisson", true, false);
        Ingredient fromage = new Ingredient("fromage", true, false);

        ArrayList listIngredients = new ArrayList<Ingredient>();
        listIngredients.add(salade);
        listIngredients.add(tomate);
        listIngredients.add(oignon);
        listIngredients.add(poisson);
        listIngredients.add(fromage);

        Sauce sauce = new Sauce("Algerienne", true, true);

        Kebab kebab = new Kebab(listIngredients, sauce);

        System.out.println(kebab.isVegetarian());
        kebab.sansOignon();
        kebab.supplementFromage();

        for (int i = 0; i < kebab.ingredients.size(); i++){
            System.out.println(kebab.ingredients.get(i).name);
        }
    }
}
