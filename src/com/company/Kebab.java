package com.company;
import java.util.ArrayList;




public class Kebab {

    public ArrayList<Ingredient> ingredients;
    public Sauce sauce;

    public Kebab(ArrayList<Ingredient> ingredients, Sauce sauce){
        this.ingredients = ingredients;
        this.sauce = sauce;
    }

    public boolean isVegetarian(){
        for (int i = 0; i < this.ingredients.size(); i++){
            if(this.ingredients.get(i).isVegetarian == false){
                return false;
            }
        }

        if(!this.sauce.isVegetarian){
            return false;
        }



        return true;
    }

    public boolean isPescetarian(){
        for (int i = 0; i < this.ingredients.size(); i++){
            if(this.ingredients.get(i).isPescetarian == false){
                return false;
            }
        }

        if(!this.sauce.isPescetarian){
            return false;
        }

        return true;
    }

    public void sansOignon(){
        for (int i = 0; i < this.ingredients.size(); i++){
            if(this.ingredients.get(i).name == "oignon"){
                this.ingredients.remove(i);
            }
        }

    }


    public void supplementFromage(){

        boolean addFromage = false;
        for (int i = 0; i < this.ingredients.size(); i++){
            if(this.ingredients.get(i).name == "fromage"){
                addFromage = true;
            }
        }

        if (addFromage){
            this.ingredients.add(new Ingredient("fromage", true, true));
        }

    }

}
