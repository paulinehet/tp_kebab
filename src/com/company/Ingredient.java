package com.company;

public class Ingredient {
    public String name;
    public Boolean isVegetarian;
    public Boolean isPescetarian;

    //TODO : faire des autres classes qui héritent de ingrédient pour gérer différents types

    public Ingredient(String name, Boolean isVegetarian, Boolean isPescetarian){
        this.name = name;
        this.isVegetarian = isVegetarian;
        this.isPescetarian = isPescetarian;
        //TODO : Add Quantité
    }
}
